package com.aashis.demoapp;

import android.content.Context;
import android.content.SharedPreferences;

public class SchedulePreference {


    private static final String slat = "lat";
    private static final String slon = "lon";
    private static final String sphone_number = "phonenumber";
    public static final String USER_LOGIN_STATUS = "login_status";


    private Context mcontext;
    private final SharedPreferences msharedpreferences;
    private SharedPreferences.Editor editor;

    public SchedulePreference(Context mcontext) {
        this.mcontext = mcontext;
        this.msharedpreferences = mcontext.getSharedPreferences(slat, Context.MODE_PRIVATE);
    }


    public void setlat(String lat) {
        this.editor = msharedpreferences.edit();
        editor.putString(slat, lat);
        editor.apply();
    }

    public String getlat() {
        return msharedpreferences.getString(slat, null);
    }

    public void setlon(String lon) {
        this.editor = msharedpreferences.edit();
        editor.putString(slon, lon);
        editor.apply();
    }

    public String getlon() {
        return msharedpreferences.getString(slon, null);
    }

    public void setUserLoggedInStatus(Boolean value) {
        this.editor = msharedpreferences.edit();
        editor.putBoolean(USER_LOGIN_STATUS, value);
        editor.apply();
    }

    public boolean getUserLoggedInStatus() {
        return msharedpreferences.getBoolean(USER_LOGIN_STATUS, false);
    }


    public void removeall() {
        this.editor = msharedpreferences.edit();
        editor.remove("lat");
        editor.remove("lon");

        editor.apply();
    }

    public void Logout() {
        this.editor = msharedpreferences.edit();
        editor.remove("USER_LOGIN_STATUS");

        editor.apply();
    }

}
