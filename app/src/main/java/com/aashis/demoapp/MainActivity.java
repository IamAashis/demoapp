package com.aashis.demoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.aashis.demoapp.Room.AddRoom;
import com.aashis.demoapp.Room.HomeRoom;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.toolbarhomepage)
    Toolbar toolbar_nav;

    private ActionBarDrawerToggle toggle;
    private ActionBar toolbar;

    @BindView(R.id.navigation)
    NavigationView navigationview;

    SchedulePreference schedulePreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        schedulePreference = new SchedulePreference(getApplicationContext());

        // Toolbar in Appbars
        toolbar = getSupportActionBar();

        Menu menu = navigationview.getMenu();

        toolbar_nav.setTitle("Find Room");
        toolbar_nav.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar_nav);

        // Side Navigation drawer
        toggle = new ActionBarDrawerToggle(MainActivity.this, drawer, toolbar_nav, R.string.open, R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //calls fragment Add room Container
//getSupportFragmentManager().beginTransaction().replace(R.id.container, new AddRoom(),"Ad_room").addToBackStack(null).commitAllowingStateLoss();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeRoom(), "Ad_room").addToBackStack(null).commitAllowingStateLoss();


        navigationview.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.add_room:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AddRoom(), "Ad_room").addToBackStack(null).commitAllowingStateLoss();
                        drawer.closeDrawers();
                        break;

                    case R.id.home:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeRoom(), "Ad_room").addToBackStack(null).commitAllowingStateLoss();
                        drawer.closeDrawers();
                        break;

                    case R.id.logout:
//                        schedulePreference.Logout();
//                        System.out.println(schedulePreference.getUserLoggedInStatus());
//                        finish();
//                        System.exit(0);
                        break;

                }

                return true;
            }
        });

    }


}