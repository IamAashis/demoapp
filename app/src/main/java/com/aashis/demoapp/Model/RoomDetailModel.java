package com.aashis.demoapp.Model;

import android.graphics.Bitmap;

public class RoomDetailModel {

    String location, map_lat, map_lon, contact, detail, two_wheeler, four_wheeler, termsandcodi, otherexpenses, posteddate, total_room;

    Bitmap image;

    public RoomDetailModel() {
    }

    public String getPosteddate() {
        return posteddate;
    }

    public void setPosteddate(String posteddate) {
        this.posteddate = posteddate;
    }

    public String getTotal_room() {
        return total_room;
    }

    public void setTotal_room(String total_room) {
        this.total_room = total_room;
    }

    public String getTermsandcodi() {
        return termsandcodi;
    }

    public void setTermsandcodi(String termsandcodi) {
        this.termsandcodi = termsandcodi;
    }

    public String getOtherexpenses() {
        return otherexpenses;
    }

    public void setOtherexpenses(String otherexpenses) {
        this.otherexpenses = otherexpenses;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMap_lat() {
        return map_lat;
    }

    public void setMap_lat(String map_lat) {
        this.map_lat = map_lat;
    }

    public String getMap_lon() {
        return map_lon;
    }

    public void setMap_lon(String map_lon) {
        this.map_lon = map_lon;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTwo_wheeler() {
        return two_wheeler;
    }

    public void setTwo_wheeler(String two_wheeler) {
        this.two_wheeler = two_wheeler;
    }

    public String getFour_wheeler() {
        return four_wheeler;
    }

    public void setFour_wheeler(String four_wheeler) {
        this.four_wheeler = four_wheeler;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}