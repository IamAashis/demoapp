package com.aashis.demoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.aashis.demoapp.LoginPage.SigninActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    public static int splash_time = 2500;

    @BindView(R.id.company_logo)
    ImageView logoimage;

    @BindView(R.id.tv_company_name)
    TextView name;

    Animation aniText, aniImage;

    SchedulePreference schedulePreference;

    Boolean login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        schedulePreference = new SchedulePreference(getApplicationContext());

        aniText = AnimationUtils.loadAnimation(this, R.anim.from_bottom);
        aniImage = AnimationUtils.loadAnimation(this, R.anim.from_bottom_second);

        name.setAnimation(aniText);
        logoimage.setAnimation(aniImage);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        login = schedulePreference.getUserLoggedInStatus();


        System.out.println("Login status value" + login);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (login.equals(true)) {

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();


                } else {
                    Intent ii = new Intent(getApplicationContext(), SigninActivity.class);
                    startActivity(ii);
                    finish();
//                    getSupportFragmentManager().beginTransaction().replace(R.id.container,new TableHome()).addToBackStack(null).addToBackStack(null).commitAllowingStateLoss();

                }
            }
        }, splash_time);


    }
}