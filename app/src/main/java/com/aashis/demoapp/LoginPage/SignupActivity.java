package com.aashis.demoapp.LoginPage;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.aashis.demoapp.DataBase.Database;
import com.aashis.demoapp.MainActivity;
import com.aashis.demoapp.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.layer1)
    TextInputLayout layer1username;

    @BindView(R.id.etUsername)
    EditText etusername;

    @BindView(R.id.layer2)
    TextInputLayout layer2mobilenuber;

    @BindView(R.id.etMobilenumber)
    EditText etmobilenumber;

    @BindView(R.id.layer3)
    TextInputLayout layer3address;

    @BindView(R.id.etAddress)
    EditText etaddress;

    @BindView(R.id.layer4)
    TextInputLayout layer4email;

    @BindView(R.id.etEmail)
    EditText etemail;

    @BindView(R.id.layer5)
    TextInputLayout layer5dob;

    @BindView(R.id.etDateofBirth)
    EditText etdateofbirth;

    @BindView(R.id.layer6)
    TextInputLayout layer6gender;

    @BindView(R.id.age_spinner)
    AutoCompleteTextView agespinner;

    @BindView(R.id.layer7)
    TextInputLayout layer7password;

    @BindView(R.id.etPassword)
    EditText etpassword;

    String[] Gender = {"Male", "Female", "Others"};
    String selectedItem;

    String IPpassword, IPmobilenumber, IPaddress, IPemail, IPdob, IPAgespinner, IPusername;
    DatePickerDialog.OnDateSetListener setListener;
    int year, month, day;

    Database database;

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);

        database = new Database(getApplicationContext());

        Calendar calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Gender);

        agespinner.setAdapter(ad);

        agespinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItem = String.valueOf(adapterView.getItemAtPosition(i));
            }
        });

    }


    @OnClick({R.id.creatalcbtn, R.id.close_img, R.id.etDateofBirth})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.creatalcbtn:
                ConfirmInput(view);
                break;
            case R.id.close_img:
                onBackPressed();
                break;
            case R.id.etDateofBirth:
                DatePicker();
                break;
        }
    }

    public void ConfirmInput(View view) {
        if (!validatename() | !validatephone() | !validateaddress() | !validatedob() | !validategender() | !validateemail() | !validatepassword()) {
            return;
        }

        Toast.makeText(this, "Yes it is Validate", Toast.LENGTH_SHORT).show();
        InsertUser();

    }

    public void InsertUser() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("full_name", IPusername);
        contentValues.put("mobile_number", IPmobilenumber);
        contentValues.put("address", IPaddress);
        contentValues.put("email", IPemail);
        contentValues.put("date_of_birth", IPdob);
        contentValues.put("gender", IPAgespinner);
        contentValues.put("password", IPpassword);

        database.InsertUser(contentValues);

        Intent GotoHome = new Intent(SignupActivity.this, MainActivity.class);
        startActivity(GotoHome);

    }

    public void DatePicker() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                SignupActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                setListener,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        long now = System.currentTimeMillis() - 1000;
        dialog.getDatePicker().setMaxDate(now + (1000 * 60 * 60 * 24 * 7));
        dialog.show();


        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                etdateofbirth.setText(String.valueOf(i + "-" + i1 + "-" + i2));

            }
        };


    }


    public boolean validatename() {
        IPusername = etusername.getText().toString().trim();

        if (IPusername.isEmpty()) {
            layer1username.setError("Field can't be empty");
            return false;
        } else {
            layer1username.setError(null);
            return true;
        }
    }

    public boolean validatephone() {
        IPmobilenumber = etmobilenumber.getText().toString().trim();

        if (IPmobilenumber.isEmpty()) {
            layer2mobilenuber.setError("Field can't be empty");
            return false;
        } else if (IPmobilenumber.length() < 10) {
            layer2mobilenuber.setError("Invalid number");
            return false;
        } else {
            layer2mobilenuber.setError(null);
            return true;
        }

    }

    public boolean validateaddress() {

        IPaddress = etaddress.getText().toString().trim();

        if (IPaddress.isEmpty()) {
            layer3address.setError("Field can't be empty");
            return false;
        } else {
            layer3address.setError(null);
            return true;
        }
    }

    public boolean validateemail() {
        IPemail = etemail.getText().toString().trim();

        if (IPemail.isEmpty()) {
            layer4email.setError("Field can't be empty");
            return false;
        } else {
            layer4email.setError(null);
            return true;
        }
    }

    public boolean validatedob() {

        IPdob = etdateofbirth.getText().toString().trim();

        if (IPdob.isEmpty()) {
            layer5dob.setError("Field can't be empty");
            return false;
        } else {
            layer5dob.setError(null);
            return true;
        }
    }

    public boolean validategender() {
        IPAgespinner = selectedItem;

        if (IPAgespinner == null) {
            layer6gender.setError("Field can't be empty");
            return false;
        } else {
            layer6gender.setError(null);
            return true;
        }
    }

    public boolean validatepassword() {

        IPpassword = etpassword.getText().toString().trim();

        if (IPpassword.isEmpty()) {
            layer7password.setError("Field can't be empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(IPpassword).matches()) {
            layer7password.setError("Weak Password");
            return false;
        } else {
            layer7password.setError(null);
            return true;
        }
    }

}