package com.aashis.demoapp.LoginPage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.aashis.demoapp.DataBase.Database;
import com.aashis.demoapp.LoadingDialog;
import com.aashis.demoapp.MainActivity;
import com.aashis.demoapp.R;
import com.aashis.demoapp.SchedulePreference;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends AppCompatActivity {

    @BindView(R.id.layer1L)
    TextInputLayout layer1mobilenumber;

    @BindView(R.id.LoginetMobilenumber)
    EditText etmobilenumber;

    @BindView(R.id.layer2L)
    TextInputLayout layer2password;

    @BindView(R.id.LoginetPassword)
    EditText etpassword;

    @BindView(R.id.saveLoginCheckBox)
    CheckBox savelogincheckbox;

    String IPphonenumber, IPpassword;

    Database database;

    SchedulePreference schedulePreference;

    Boolean checklogin;

    LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ButterKnife.bind(this);
        loadingDialog = new LoadingDialog(SigninActivity.this);


        database = new Database(getApplicationContext());
        schedulePreference = new SchedulePreference(getApplicationContext());

        checklogin = savelogincheckbox.isChecked();


        savelogincheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checklogin = savelogincheckbox.isChecked();

                System.out.println("Check Login" + checklogin);
            }
        });

    }

    @OnClick({R.id.gotosignup, R.id.btnskipfornow, R.id.login_btn})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.gotosignup:
                Intent GotoSignup = new Intent(SigninActivity.this, SignupActivity.class);
                startActivity(GotoSignup);
                break;

            case R.id.btnskipfornow:
                Intent gotohome = new Intent(SigninActivity.this, MainActivity.class);
                startActivity(gotohome);
                break;

            case R.id.login_btn:
                loadingDialog.startloadingDialog();
                confirmInput(view);

        }
    }


    public boolean validatephone() {
        IPphonenumber = etmobilenumber.getText().toString().trim();

        if (IPphonenumber.isEmpty()) {
            layer1mobilenumber.setError("Field can't be empty");
            return false;
        } else if (IPphonenumber.length() < 10) {
            layer1mobilenumber.setError("Invalid number");
            return false;
        } else {
            layer1mobilenumber.setError(null);
            return true;
        }
    }


    public boolean validatepassword() {

        IPpassword = etpassword.getText().toString().trim();

        if (IPpassword.isEmpty()) {
            layer2password.setError("Field can't be empty");
            return false;
        } else {
            layer2password.setError(null);
            return true;
        }
    }

    public void confirmInput(View view) {
        if (!validatephone() | !validatepassword()) {

            loadingDialog.dismissDialog();

            return;

        }

        if (database.isLoginValid(IPphonenumber, IPpassword)) {
            Intent GotoHome = new Intent(SigninActivity.this, MainActivity.class);
            startActivity(GotoHome);
            loadingDialog.dismissDialog();

            schedulePreference.setUserLoggedInStatus(checklogin);

//            schedulePreference.setphonenumber(IPphonenumber);

            finish();
        } else {
            loadingDialog.dismissDialog();

            Toast.makeText(this, "Password or phone number is InCorrect", Toast.LENGTH_SHORT).show();
        }


    }

}