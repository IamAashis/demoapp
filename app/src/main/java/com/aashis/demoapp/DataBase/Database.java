package com.aashis.demoapp.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.Nullable;

import com.aashis.demoapp.Model.RoomDetailModel;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {

    private static final int Database_version = 1; //Database Version
    private static final String Database_name = "roomdbs"; //Database Name

    String querycustomer = "CREATE TABLE if NOT EXISTS \"RoomDB\" (\n" +
            "\t\"customer_id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\t\"full_name\"\tTEXT,\n" +
            "\t\"mobile_number\"\tTEXT,\n" +
            "\t\"address\"\tTEXT,\n" +
            "\t\"email\"\tTEXT,\n" +
            "\t\"date_of_birth\"\tTEXT,\n" +
            "\t\"gender\"\tTEXT,\n" +
            "\t\"password\"\tTEXT\n" +
            ");";

    String queryroomdetail = "CREATE TABLE IF NOT EXISTS \"roomdetail\" (\n" +
            "\t\"room_id\"\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\t\"location\"\tTEXT,\n" +
            "\t\"map_lat\"\tTEXT,\n" +
            "\t\"map_long\"\tTEXT,\n" +
            "\t\"contact_number\"\tTEXT,\n" +
            "\t\"image\"\tBLOB,\n" +
            "\t\"detail\"\tTEXT,\n" +
            "\t\"termsconditions\"\tTEXT,\n" +
            "\t\"p_twowheeler\"\tTEXT,\n" +
            "\t\"p_fourwheeler\"\tTEXT,\n" +
            "\t\"customer_id\"\tINTEGER,\n" +
            "\t\"total_room\"\tTEXT,\n" +
            "\t\"date\"\tTEXT,\n" +
            "\t\"others_expenses\"\tTEXT,\n" +
            "\tFOREIGN KEY(\"customer_id\") REFERENCES \"RoomDB\"(\"customer_id\")\n" +
            ");";

    public Database(Context context) {
        super(context, Database_name, null, Database_version);
        getWritableDatabase().execSQL(querycustomer);
        getWritableDatabase().execSQL(queryroomdetail);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(querycustomer);
        sqLiteDatabase.execSQL(queryroomdetail);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public void InsertUser(ContentValues contentValues) {
        getWritableDatabase().insert("RoomDB", "", contentValues);
        getWritableDatabase().close();
    }

    public void InsertRoomDetail(ContentValues contentValues) {
        getWritableDatabase().insert("roomdetail", "", contentValues);
        getWritableDatabase().close();
    }

    public boolean isLoginValid(String phonenumber, String password) {
        String loginquery = "Select count(*) from RoomDB where mobile_number='" + phonenumber +
                "' and password = '" + password + "'";

        SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(loginquery);
        long l = sqLiteStatement.simpleQueryForLong();

        if (l == 1) {
            return true;
        }
        return false;

    }

    public List<RoomDetailModel> getAllRoomDetail() {
        List<RoomDetailModel> roomDetailModels = new ArrayList<>();

        String selectQuery = "SELECT * FROM roomdetail";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                RoomDetailModel roomDetailModel = new RoomDetailModel();

                roomDetailModel.setLocation(cursor.getString(cursor.getColumnIndex("location")));
                roomDetailModel.setMap_lat(cursor.getString(cursor.getColumnIndex("map_lat")));
                roomDetailModel.setMap_lon(cursor.getString(cursor.getColumnIndex("map_long")));
                roomDetailModel.setContact(cursor.getString(cursor.getColumnIndex("contact_number")));
                byte[] imagesbytes = (cursor.getBlob(cursor.getColumnIndex("image")));
                Bitmap objectBitmap = BitmapFactory.decodeByteArray(imagesbytes, 0, imagesbytes.length);
                roomDetailModel.setImage(objectBitmap);
                roomDetailModel.setDetail(cursor.getString(cursor.getColumnIndex("detail")));
                roomDetailModel.setTermsandcodi(cursor.getString(cursor.getColumnIndex("termsconditions")));
                roomDetailModel.setOtherexpenses(cursor.getString(cursor.getColumnIndex("others_expenses")));
                roomDetailModel.setTotal_room(cursor.getString(cursor.getColumnIndex("total_room")));
                roomDetailModel.setPosteddate(cursor.getString(cursor.getColumnIndex("date")));
                roomDetailModel.setTwo_wheeler(cursor.getString(cursor.getColumnIndex("p_twowheeler")));
                roomDetailModel.setFour_wheeler(cursor.getString(cursor.getColumnIndex("p_fourwheeler")));

                roomDetailModels.add(roomDetailModel);
            } while (cursor.moveToNext());
        }

        db.close();

        return roomDetailModels;

    }


}
