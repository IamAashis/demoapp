package com.aashis.demoapp.search;

import android.widget.Filter;

import com.aashis.demoapp.Adapter.RoomDetailAdapter;
import com.aashis.demoapp.Model.RoomDetailModel;


import java.util.ArrayList;
import java.util.List;

public class RoomSearch extends Filter {

    RoomDetailAdapter roomDetailAdapter;
    List<RoomDetailModel> roomDetailModels;

    public RoomSearch(RoomDetailAdapter roomDetailAdapter, List<RoomDetailModel> roomDetailModels) {
        this.roomDetailAdapter = roomDetailAdapter;
        this.roomDetailModels = roomDetailModels;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {

        FilterResults results = new FilterResults();

        //CHECK CONSTRAINT VALIDITY
        if (charSequence != null && charSequence.length() > 0) {
            //CHANGE TO UPPER
            charSequence = charSequence.toString().toUpperCase();
            //STORE OUR FILTERED list
            ArrayList<RoomDetailModel> filteredDate = new ArrayList<>();

            for (int i = 0; i < roomDetailModels.size(); i++) {
                //CHECK
                if (roomDetailModels.get(i).getTotal_room().toUpperCase().contains(charSequence)) {
                    //ADD list TO FILTERED room
                    filteredDate.add(roomDetailModels.get(i));
                }
            }

            results.count = filteredDate.size();
            results.values = filteredDate;
        } else {
            results.count = roomDetailModels.size();
            results.values = roomDetailModels;

        }
        return results;

    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        // adapter.contactUsModels = (ArrayList<BivagcatMiddle>) filterResults.values;
        roomDetailAdapter.roomDetailModelList = (ArrayList<RoomDetailModel>) filterResults.values;

        //REFRESH
        roomDetailAdapter.notifyDataSetChanged();
    }


}
