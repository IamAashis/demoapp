package com.aashis.demoapp.Room;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.aashis.demoapp.R;
import com.aashis.demoapp.SchedulePreference;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Chooselocation extends AppCompatActivity {

    private GoogleMap mMap;

    String IPlong, IPlat;

    SchedulePreference schedulePreference;

    @BindView(R.id.btnchangemap)
    Button btnchangemap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_fragment);
        ButterKnife.bind(this);

        schedulePreference = new SchedulePreference(getApplicationContext());

        SupportMapFragment mapDetail = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.choose_map));
        mapDetail.getMapAsync(this::onMapReady);

    }


    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;

        // I suppressed the missing-permission warning because this wouldn't be executed in my
        // case without location services being enabled
        @SuppressLint("MissingPermission") android.location.Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        double userLat = lastKnownLocation.getLatitude();
        double userLong = lastKnownLocation.getLongitude();

//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Maker in sydney"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(userLat, userLong)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLat, userLong), 15));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng).title("Is this your location"));
                System.out.println("lat" + latLng.latitude + "Lang" + latLng.longitude);
                IPlong = String.valueOf(latLng.longitude);
                IPlat = String.valueOf(latLng.latitude);

                schedulePreference.setlat(IPlat);
                schedulePreference.setlon(IPlong);
            }
        });


        btnchangemap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL ? GoogleMap.MAP_TYPE_HYBRID :
                        GoogleMap.MAP_TYPE_NORMAL);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Chooselocation.this);
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogview = LayoutInflater.from(Chooselocation.this).inflate(R.layout.custom_map, viewGroup, false);

                Button btn_okay = dialogview.findViewById(R.id.btn_okay);


                builder.setView(dialogview);
                AlertDialog alertDialog = builder.create();


                btn_okay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        onBackPressed();
                    }
                });


                alertDialog.show();

                return false;
            }
        });

        setlocationenable();


    }


    public void setlocationenable() {
        if (ActivityCompat.checkSelfPermission(Chooselocation.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Chooselocation.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        setlocationenable();
    }
}
