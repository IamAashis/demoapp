package com.aashis.demoapp.Room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aashis.demoapp.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoomDetailActivity extends AppCompatActivity {

    String Rlocation, Rmap_lat, Rmap_lon, Rcontact, Rdetail, Rtwo_wheeler, Rfour_wheeler, Rtermsandcodi, Rtotal_room, Rotherexpenses, Rposteddate;
    byte[] Rimage;
    Bitmap bmp;
    private static int REQUEST_CODE_PERMISSION = 1;

    @BindView(R.id.app_bar)
    Toolbar title;

    @BindView(R.id.Char_Image)
    ImageView Char_Image;

    @BindView(R.id.rdroom_heading)
    TextView rdroom_heading;

    @BindView(R.id.rdposteddate)
    TextView rdposteddate;

    @BindView(R.id.rdlocation)
    TextView rdlocation;

    @BindView(R.id.rdcontact)
    TextView rdcontact;

    @BindView(R.id.rdroom_detail)
    TextView rdroom_detail;

    @BindView(R.id.rdother_charges)
    TextView rdother_charges;

    @BindView(R.id.rdparkingtwoimg)
    ImageView rdparkingtwoimg;

    @BindView(R.id.rdparkingfourimg)
    ImageView rdparkingfourimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        Rlocation = bundle.getString("location");
        Rmap_lat = bundle.getString("map_lat");
        Rmap_lon = bundle.getString("map_lon");
        Rcontact = bundle.getString("contact");
        Rdetail = bundle.getString("detail");
        Rtwo_wheeler = bundle.getString("two_wheeler");
        Rfour_wheeler = bundle.getString("four_wheeler");
        Rtermsandcodi = bundle.getString("termsandcodi");
        Rotherexpenses = bundle.getString("otherexpenses");
        Rposteddate = bundle.getString("posteddate");
        Rtotal_room = bundle.getString("total_room");
        Rimage = bundle.getByteArray("image");

        title.setTitle(Rtotal_room);
        bmp = BitmapFactory.decodeByteArray(Rimage, 0, Rimage.length);
        Char_Image.setImageBitmap(bmp);
        rdroom_heading.setText(Rtotal_room);
        rdposteddate.setText("Posted Date " + Rposteddate);
        rdlocation.setText(Rlocation);
        rdcontact.setText(Rcontact);
        rdroom_detail.setText(Rdetail);
        rdother_charges.setText(Rotherexpenses);

        setSupportActionBar(title);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true); // Back icon in coll...


        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        if (Rtwo_wheeler.equals("1")) {
            rdparkingtwoimg.setImageResource(R.drawable.ic_baseline_check_circle);
        } else {
            rdparkingtwoimg.setImageResource(R.drawable.ic_baseline_remove_circle);

        }
        if (Rfour_wheeler.equals("1")) {
            rdparkingfourimg.setImageResource(R.drawable.ic_baseline_check_circle);
        } else {
            rdparkingfourimg.setImageResource(R.drawable.ic_baseline_remove_circle);

        }

    }



    @OnClick({R.id.calllayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.calllayout:
                if (ContextCompat.checkSelfPermission(RoomDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) RoomDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CODE_PERMISSION);

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Rcontact));
                    startActivity(callIntent);


                } else {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Rcontact));
                    startActivity(callIntent);
                }
                break;
        }
    }
}