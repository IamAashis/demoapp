package com.aashis.demoapp.Room;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aashis.demoapp.Adapter.RoomDetailAdapter;
import com.aashis.demoapp.DataBase.Database;
import com.aashis.demoapp.Listener.RoomDetailInterface;
import com.aashis.demoapp.Model.RoomDetailModel;
import com.aashis.demoapp.R;
import com.aashis.demoapp.search.RoomSearch;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeRoom extends Fragment {

    @BindView(R.id.Recyleviehome)
    RecyclerView recyclerViewhome;

    @BindView(R.id.type)
    CardView changemap;

    @BindView(R.id.searchedit)
    EditText searchroom;

    Database database;
    ArrayList<RoomDetailModel> roomDetailModels = new ArrayList<>();

    RoomDetailAdapter roomDetailAdapter;
    private GoogleMap mMap;
    MarkerOptions markerOptions;

    Location selected_location;

    RoomDetailInterface roomDetailInterface;

    RoomSearch roomSearch;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.roomhome, container, false);
        ButterKnife.bind(this, rootview);
        database = new Database(getContext());

        roomDetailModels.removeAll(roomDetailModels);
        roomDetailModels.addAll(database.getAllRoomDetail());

        //// CHeck this
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map_home);

        mapFragment.getMapAsync(this::onMapReady);
        markerOptions = new MarkerOptions();

        Detail();
        roomDetailAdapter = new RoomDetailAdapter(roomDetailModels, roomDetailInterface, getContext());
        recyclerViewhome.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewhome.setHasFixedSize(true);
        recyclerViewhome.setAdapter(roomDetailAdapter);


        searchroom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                roomDetailAdapter.getFilter().filter(searchroom.getText().toString());

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return rootview;
    }

    public void map() {
        mMap.clear();
        for (int i = 0; i < roomDetailModels.size(); i++) {
            RoomDetailModel roomDetailModel = roomDetailModels.get(i);

            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.home);

            System.out.println("lat ang lat value" + Double.valueOf(roomDetailModel.getMap_lat()) + Double.valueOf(roomDetailModel.getMap_lon()));

            LatLng latLng = new LatLng(Double.parseDouble(roomDetailModel.getMap_lat()), Double.parseDouble(roomDetailModel.getMap_lon()));
            System.out.println(latLng);
            markerOptions.position(latLng);
            markerOptions.title(roomDetailModel.getTotal_room());
            markerOptions.icon(icon);
            mMap.addMarker(markerOptions).showInfoWindow();


        }


    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getMaxZoomLevel();
        mMap.getMinZoomLevel();
        mMap.getUiSettings();
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomOut());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        setlocationenable();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Maker in sydney"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(27.687445, 85.65412)).title("Marker"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(27.687445, 85.65412), 10));

        changemap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(mMap.getMapType() == GoogleMap.MAP_TYPE_HYBRID
                        ? googleMap.MAP_TYPE_NORMAL : GoogleMap.MAP_TYPE_HYBRID
                );
            }
        });


        map();


    }

    public void setlocationenable() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        setlocationenable();
    }

    public void Detail() {
        roomDetailInterface = new RoomDetailInterface() {
            @Override
            public void roomdetailinterfa(String location, String map_lat, String map_lon, String contact, String detail,
                                          String two_wheeler, String four_wheeler, String termsandcodi, String otherexpenses,
                                          String posteddate, String total_room, Bitmap image) {
                byte[] bytes = null;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bytes = stream.toByteArray();

                Bundle bundle = new Bundle();
                bundle.putString("location", location);
                bundle.putString("map_lat", map_lat);
                bundle.putString("map_lon", map_lon);
                bundle.putString("contact", contact);
                bundle.putString("detail", detail);
                bundle.putString("two_wheeler", two_wheeler);
                bundle.putString("four_wheeler", four_wheeler);
                bundle.putString("termsandcodi", termsandcodi);
                bundle.putString("otherexpenses", otherexpenses);
                bundle.putString("posteddate", posteddate);
                bundle.putString("total_room", total_room);
                bundle.putByteArray("image", bytes);

                Intent goToRoomDetail = new Intent(getContext(), RoomDetailActivity.class);
                goToRoomDetail.putExtras(bundle);
                startActivity(goToRoomDetail);

            }
        };
    }

}
