package com.aashis.demoapp.Room;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.aashis.demoapp.DataBase.Database;
import com.aashis.demoapp.ImageResizer;
import com.aashis.demoapp.MainActivity;
import com.aashis.demoapp.R;
import com.aashis.demoapp.SchedulePreference;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class AddRoom extends Fragment {

    private GoogleMap mMap;

    Database database;

    @BindView(R.id.layer1AR)
    TextInputLayout l1loation;

    @BindView(R.id.aretlocation)
    EditText location;

    @BindView(R.id.layer2AR)
    TextInputLayout l2price;

    @BindView(R.id.aretprice)
    EditText price;

    @BindView(R.id.layer3AR)
    TextInputLayout l3contact;

    @BindView(R.id.aretcontact)
    EditText contact;

    @BindView(R.id.layer5AR)
    TextInputLayout l5detail;

    @BindView(R.id.aretroomdesc)
    EditText roomdetail;

    @BindView(R.id.layer4AR)
    TextInputLayout l4termcondi;

    @BindView(R.id.arettermcondi)
    EditText termscondition;

    @BindView(R.id.layer6AR)
    TextInputLayout l6otherexpenses;

    @BindView(R.id.arettotherexpeses)
    EditText otherexpenses;

    @BindView(R.id.showimage)
    ImageView showimage;

    @BindView(R.id.cbtwowheeler)
    CheckBox cbtwowheeler;

    @BindView(R.id.cbfourwheeler)
    CheckBox cbfourwheeler;

    @BindView(R.id.layer8AR)
    TextInputLayout l8totalrooms;

    @BindView(R.id.arettotalrooms)
    EditText totalrooms;


    String IPlocation, IPprice, IPcontact, IPdetail, IPtermscondi, IPtwowheller, IPfourwheeler, IPtotalrooms, IPpostdate, IPotherexp;
    double IPlong, IPlat;

    private static final int SELECT_PICTURE = 100;

    private Uri imagefilepath;

    private Bitmap imagetostore;

    byte[] imagesintobyte;
    Calendar calendar;

    SchedulePreference schedulePreference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.add_room, container, false);
        ButterKnife.bind(this, rootview);

        database = new Database(getActivity());

        schedulePreference = new SchedulePreference(getContext());
        calendar = Calendar.getInstance();
        IPpostdate = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar
                .DAY_OF_MONTH);


        return rootview;

    }

    @OnClick({R.id.btn_roomlocation, R.id.room_image, R.id.showimage, R.id.btnsubmitroom})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.btn_roomlocation:
                Intent gotomap = new Intent(getContext(), Chooselocation.class);
                startActivity(gotomap);
//                map();
                break;

            case R.id.room_image:
                try {
                    Intent objectIntent = new Intent();
                    objectIntent.setType("image/*");
                    objectIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(objectIntent, SELECT_PICTURE);
                    break;
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.btnsubmitroom:
                ConfirmInput(view);
                break;
        }
    }


    public void insertRoomDet() {

        if (cbtwowheeler.isChecked()) {
            IPtwowheller = "1"; //Available
        } else {
            IPtwowheller = "0";
        }

        if (cbfourwheeler.isChecked()) {
            IPfourwheeler = "1";
        } else {
            IPfourwheeler = "0";
        }

        if (schedulePreference.getlat() != null) {
            IPlat = Double.parseDouble(schedulePreference.getlat());
            IPlong = Double.parseDouble(schedulePreference.getlon());
        }

        System.out.println("lat.... value in it" + schedulePreference.getlat());

        Bitmap fullsizeBitmap = imagetostore;

        Bitmap reduceBitmap = ImageResizer.reduceBitmapSize(fullsizeBitmap,
                340000);

        File file = new File(Environment.getExternalStorageState() + File.separator + "reducefile");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        reduceBitmap.compress(Bitmap.CompressFormat.JPEG, 30, bos);
        imagesintobyte = bos.toByteArray();

        ContentValues contentValues = new ContentValues();
        contentValues.put("location", IPlocation);
        contentValues.put("map_lat", IPlat);
        contentValues.put("map_long", IPlong);
        contentValues.put("contact_number", IPcontact);
        contentValues.put("image", imagesintobyte);
        contentValues.put("detail", IPdetail);
        contentValues.put("termsconditions", IPtermscondi);
        contentValues.put("p_twowheeler", IPtwowheller);
        contentValues.put("p_fourwheeler", IPfourwheeler);
        contentValues.put("customer_id", "");
        contentValues.put("total_room", IPtotalrooms);
        contentValues.put("date", IPpostdate);
        contentValues.put("others_expenses", IPotherexp);

        database.InsertRoomDetail(contentValues);
        schedulePreference.removeall();
        Toast.makeText(getContext(), "Room Added Sucessfully", Toast.LENGTH_SHORT).show();

        Intent gotomain = new Intent(getContext(), MainActivity.class);
        startActivity(gotomain);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && data != null && data.getData() != null) {
                imagefilepath = data.getData();
                imagetostore = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imagefilepath);
                showimage.setImageBitmap(imagetostore);
//                showimage.setVisibility(View.VISIBLE);
            }

        } catch (IOException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //Validation
    public boolean validateimage() {

        System.out.println("image to store"+ imagetostore);
        if (imagetostore == null) {
            Toast.makeText(getContext(), "Please Choose the Image", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean validatelocation() {
        IPlocation = location.getText().toString().trim();

        if (IPlocation.isEmpty()) {
            l1loation.setError("Field can't be empty");
            return false;
        } else {
            l1loation.setError(null);
            return true;
        }
    }

    public boolean validatetotalroom() {
        IPtotalrooms = totalrooms.getText().toString().trim();

        if (IPtotalrooms.isEmpty()) {
            l8totalrooms.setError("Field can't be empty");
            return false;
        } else {
            l8totalrooms.setError(null);
            return true;
        }
    }


    public boolean validateotherexp() {
        IPotherexp = otherexpenses.getText().toString().trim();

        if (IPotherexp.isEmpty()) {
            l6otherexpenses.setError("Field can't be empty");
            return false;
        } else {
            l6otherexpenses.setError(null);
            return true;
        }
    }

    public boolean validatetermandcondi() {
        IPtermscondi = termscondition.getText().toString().trim();

        if (IPtermscondi.isEmpty()) {
            l4termcondi.setError("Field can't be empty");
            return false;
        } else {
            l4termcondi.setError(null);
            return true;
        }
    }


    public boolean validatedetail() {
        IPdetail = roomdetail.getText().toString().trim();

        if (IPdetail.isEmpty()) {
            l5detail.setError("Field can't be empty");
            return false;
        } else {
            l5detail.setError(null);
            return true;
        }
    }

    public boolean validatecontact() {
        IPcontact = contact.getText().toString().trim();

        if (IPcontact.isEmpty()) {
            l3contact.setError("Field can't be empty");
            return false;
        } else {
            l3contact.setError(null);
            return true;
        }
    }


    public boolean validateprice() {
        IPprice = price.getText().toString().trim();

        if (IPprice.isEmpty()) {
            l2price.setError("Field can't be empty");
            return false;
        } else {
            l2price.setError(null);
            return true;
        }
    }


    public void ConfirmInput(View view) {
        if (!validatecontact() | !validatedetail() | !validatelocation() | !validateotherexp() |
                !validateprice() | !validatetermandcondi() | !validatetotalroom() | !validateimage()) {
            return;
        }


        insertRoomDet();

    }


}
