package com.aashis.demoapp.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aashis.demoapp.Listener.RoomDetailInterface;
import com.aashis.demoapp.Model.RoomDetailModel;
import com.aashis.demoapp.R;
import com.aashis.demoapp.search.RoomSearch;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomDetailAdapter extends RecyclerView.Adapter<RoomDetailAdapter.MyViewHolder> implements Filterable {

    public ArrayList<RoomDetailModel> roomDetailModelList, roomModelsfilter;
    RoomDetailInterface roomDetailInterface;
    RoomSearch roomSearch;
    Context context;

    public RoomDetailAdapter(ArrayList<RoomDetailModel> roomDetailModelList, RoomDetailInterface roomDetailInterface, Context context) {
        this.roomDetailModelList = roomDetailModelList;
        this.roomModelsfilter = roomDetailModelList;
        this.roomDetailInterface = roomDetailInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_roomdetail, parent, false);
        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        RoomDetailModel roomDetailModel = roomDetailModelList.get(position);

        holder.imageview.setImageBitmap(roomDetailModel.getImage());
        holder.location.setText(roomDetailModel.getLocation());
        holder.roomname.setText(roomDetailModel.getTotal_room());
        holder.contact.setText(roomDetailModel.getContact());

        holder.cardlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail(roomDetailModel.getLocation(), roomDetailModel.getMap_lat(), roomDetailModel.getMap_lon(), roomDetailModel.getContact(),
                        roomDetailModel.getDetail(), roomDetailModel.getTwo_wheeler(), roomDetailModel.getFour_wheeler(), roomDetailModel.getTermsandcodi(),
                        roomDetailModel.getOtherexpenses(), roomDetailModel.getPosteddate(), roomDetailModel.getTotal_room(), roomDetailModel.getImage());

            }
        });

    }

    public void detail(String location, String map_lat, String map_lon, String contact, String detail, String two_wheeler, String four_wheeler,
                       String termsandcodi, String otherexpenses, String posteddate, String total_room, Bitmap image) {
        roomDetailInterface.roomdetailinterfa(location, map_lat, map_lon, contact, detail, two_wheeler, four_wheeler, termsandcodi, otherexpenses,
                posteddate, total_room, image);
    }

    @Override
    public int getItemCount() {
        return roomDetailModelList.size();
    }

    @Override
    public Filter getFilter() {

        if (roomSearch == null) {
            roomSearch = new RoomSearch(this, roomModelsfilter);
        }

        return roomSearch;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.crdimageview)
        ImageView imageview;

        @BindView(R.id.crdlocation)
        TextView location;

        @BindView(R.id.crdcontact)
        TextView contact;

        @BindView(R.id.crdroomtotal)
        TextView roomname;

        @BindView(R.id.card_layout)
        CardView cardlayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
