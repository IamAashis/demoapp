package com.aashis.demoapp.Listener;

import android.graphics.Bitmap;

public interface RoomDetailInterface {

    void roomdetailinterfa(String location, String map_lat, String map_lon, String contact, String detail, String two_wheeler, String four_wheeler,
                             String termsandcodi, String otherexpenses, String posteddate, String total_room, Bitmap image);
}
